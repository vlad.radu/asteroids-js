let canvas;
let ctx;
let debug ;
let CANVAS_WIDTH = 1400;
let CANVAS_HEIGHT = 800;
const keys = [];
const bullets = [];
const asteroids = [];
let gameLevel = 1;
let pauseRender = false;
let gameOver = false;
let ship;
let score = 0;
let lives = 3;


document.documentElement.addEventListener('keydown', function (e) {
    if ( ( e.keycode || e.which ) === 32) {
        e.preventDefault();
    }
}, false);

document.addEventListener('DOMContentLoaded', SetupCanvas);
// document.addEventListener('resize', resize);

function SetupCanvas(){
  console.log('Loaded!');
  // CANVAS_HEIGHT = document.getElementById('screen').offsetHeight;
  window.addEventListener('resize',resizefunc, false);
  function resizefunc() {
    CANVAS_WIDTH = document.getElementById('screen').getBoundingClientRect().width;
    console.log(CANVAS_HEIGHT, CANVAS_WIDTH);
    canvas = document.getElementById('space');
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
  }
  resizefunc();
  // canvas.setAttribute('width', CANVAS_WIDTH);
  // canvas.setAttribute('height', CANVAS_HEIGHT);
  ctx = canvas.getContext('2d');
  // canvas.style.width = CANVAS_WIDTH;
  // canvas.style.height = CANVAS_HEIGHT;
  ctx.fillStyle = 'black';
  ctx.fillRect(0,0,canvas.width, canvas.height);
  ship = new Ship();
  spawnAsteroids(1);
  document.body.addEventListener("keydown", function (e) {
    keys[e.keyCode] = true;
    console.log("Pressed ", e);
  });
  document.body.addEventListener("keyup", function (e) {
    keys[e.keyCode] = false;
    if(e.keyCode === 80) { // p - pause
      pauseRender = !pauseRender;
      Render();
      ctx.font = '50px Arial';
      ctx.textAlign = "center";
      ctx.fillText('Paused!', CANVAS_WIDTH / 2 , CANVAS_HEIGHT / 2 );
    }
    if(e.keyCode === 32) { //space - shoot
      if (gameOver) {
        resetState();
        Render();
      }
      if (pauseRender) {
        return;
      }
      bullets.push(new Bullet(ship.angle));
    }
  });
  Render();
}

function spawnAsteroids(numAsteroids){
  for (let i = 0; i < numAsteroids; i++) {
    asteroids.push(new Asteroid());
    // console.log(asteroids[i]);
  }
}

function resetState() {
  gameOver = false;
  score = 0;
  lives = 3;
  asteroids.splice(0,asteroids.length);
  bullets.splice(0,bullets.length);
  spawnAsteroids(8);
}


class Bullet {
  constructor(angle) {
    this.visible = true;
    this.x = ship.noseX;
    this.y = ship.noseY;
    this.angle = angle;
    this.height = 4;
    this.width = 4;
    this.speed = 5;
    this.velX = 0;
    this.velY = 0;
  }

  Update() {
    let radians = this.angle / Math.PI * 180;
    this.x -= Math.cos(radians) * this.speed;
    this.y -= Math.sin(radians) * this.speed;
  }

  Draw() {
    ctx.fillStyle = 'white';
    ctx.fillRect(this.x, this.y, this.width, this.height);
  }
}

class Asteroid {
  constructor(x, y, radius, level, collisionRadius) {
    this.visible = true;
    this.x = x || Math.floor(Math.random() * CANVAS_HEIGHT);
    this.y = y || Math.floor(Math.random() * CANVAS_WIDTH);
    this.speed = 1;
    this.radius =  radius || 50;
    this.angle = Math.floor(Math.random() * 359);
    this.strokeColor = 'white';
    this.collisionRadius = collisionRadius || 46;
    this.level = level || 1;
    this.seed = Math.random();
    while (circleCollision(ship.x, ship.y, ship.collisionRadius + 40, this.x, this.y, this.collisionRadius) && this.level === 1){
      this.x = x || Math.floor(Math.random() * CANVAS_HEIGHT);
      this.y = y || Math.floor(Math.random() * CANVAS_WIDTH);
    }
  }

  Update() {
    let  radians = this.angle / Math.PI * 180;
    this.x += Math.cos(radians) * this.speed;
    this.y += Math.sin(radians) * this.speed;

    if (this.x < this.radius) {
      this.x = canvas.width;
    }

    if (this.x > canvas.width) {
      this.x = this.radius;
    }

    if (this.y < this.radius) {
      this.y = canvas.height;
    }

    if (this.y > canvas.height) {
      this.y = this.radius;
    }

  }

  Draw() {
    // let angleSeed = ((this.seed * 5) + 4);ctx.beginPath();
    // ctx.fillStyle('pink');
    if (debug){
      ctx.strokeStyle = 'pink';
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
      ctx.stroke();
    }
    ctx.strokeStyle = this.strokeColor;
    let vertAngle = ((Math.PI * 2) / 6 );
    let radians = this.angle / Math.PI * 180;
    ctx.beginPath();
    for (let i = 0; i < 6; i++ ) {
      ctx.lineTo(
        this.x - this.radius * Math.cos(vertAngle * i + radians),
        this.y - this.radius * Math.sin(vertAngle * i + radians)
      );
    }
      ctx.closePath();
      ctx.stroke();

  }
}

class Ship {
  constructor() {
    this.visible = true;
    this.invincible = false;
    this.x = CANVAS_WIDTH / 2;
    this.y = CANVAS_HEIGHT /2;
    this.collisionRadius = 14;
    this.movingForward = false;
    this.speed = 0.1;
    this.velX = 0;
    this.velY = 0;
    this.rotateSpeed = 0.001;
    this.radius = 15;
    this.angle = 0;
    this.strokeColor = 'white';
    this.noseOff = this.radius * 2;
    this.noseX = CANVAS_WIDTH / 2 + this.radius + this.noseOff;
    this.noseY = CANVAS_HEIGHT / 2 + this.radius + this.noseOff;

  }

  Rotate(dir) {
    this.angle += this.rotateSpeed * dir;
  }

  Invincibility() {
    this.invincible = true;
    let clearInvuln = setInterval(() => {
      this.visible = !this.visible;
    }, 100);
    setTimeout(() => {
      clearInterval(clearInvuln);
      this.visible = true;
      this.invincible = false;
    }, 3000);
  }

  Update() {
    let radians = this.angle / Math.PI * 180;
    if (this.movingForward) {
      this.velX += Math.cos(radians) * this.speed;
      this.velY += Math.sin(radians) * this.speed;
    }
    if (this.x < this.radius) {
      this.x = canvas.width;
    }

    if (this.x > canvas.width) {
      this.x = this.radius;
    }

    if (this.y < this.radius) {
      this.y = canvas.height;
    }

    if (this.y > canvas.height) {
      this.y = this.radius;
    }
    this.velX *= 0.99;
    this.velY *= 0.99;

    this.x -= this.velX;
    this.y -= this.velY;
  }

  Draw() {
    if (debug){
      ctx.strokeStyle = 'pink';
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
      ctx.stroke();
    }
    ctx.strokeStyle = this.strokeColor;
    const noseOff = this.radius * 2;
    let vertAngle = ((Math.PI *2) / 3);
    let radians = this.angle / Math.PI * 180;
    this.noseX = this.x - (this.radius + this.noseOff) * Math.cos(radians);
    this.noseY = this.y - (this.radius + this.noseOff) * Math.sin(radians);
    // ctx.fillRect(this.noseX, this.noseY, 4, 4);
    ctx.beginPath();
    for (let i = 0; i < 3; i++ ) {
      ctx.lineTo(
        this.x - (i === 0 ? this.radius + this.noseOff : this.radius) * Math.cos(vertAngle * i + radians),
        this.y - (i === 0 ? this.radius + this.noseOff : this.radius) * Math.sin(vertAngle * i + radians)
      );
    }
    ctx.closePath();
    ctx.stroke();
  }
}

function circleCollision ( p1x, p1y, r1, p2x, p2y, r2) {
  let xDiff;
  let yDiff;
  let radiusSum = r1 + r2;
  xDiff = p1x - p2x;
  yDiff = p1y - p2y;
  if (radiusSum > Math.sqrt((xDiff * xDiff) + (yDiff * yDiff))) {
    return true;
  }
  else {
    return false;
  }
}

function DrawLifeShips() {
  let startX = CANVAS_WIDTH - (CANVAS_WIDTH - 0.95* CANVAS_WIDTH );
  let startY = CANVAS_HEIGHT - (CANVAS_HEIGHT - 0.03* CANVAS_HEIGHT);
  let points = [[9,9], [-9,9]];
  ctx.font = '25px Monospace';
  ctx.textAlign = "center";
  ctx.fillText("Lives", startX - (0.2 * CANVAS_WIDTH ), startY + 10 );
  ctx.strokeStyle = 'white';
  for(let i = 0; i < lives; i++){
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    for(let j = 0; j < points.length; j++) {
      ctx.lineTo(startX + points[j][0], startY + points[j][1]);
    }
    ctx.closePath();
    ctx.stroke();
    startX -= 30;
  }
}
function Render() {
  if (gameOver || pauseRender) {
    console.log("BLOCKED");
    return;
  }
  let scoreElem = document.getElementById('score');
  ship.movingForward = (keys[87]);
  if(keys[68]) {
    ship.Rotate(1);
  }
  if(keys[65]) {
    ship.Rotate(-1);
  }
  ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
  if (parseInt(scoreElem.innerHTML) !== score){
    scoreElem.innerHTML = score;
  }

  if (lives <= 0) { // Game Over
    //ship.visible = false;
    ctx.fillStyle = 'white';
    ctx.font = '50px Arial';
    ctx.textAlign = "center";
    ctx.fillText('GAME OVER', CANVAS_WIDTH / 2 , CANVAS_HEIGHT / 2 );
    ctx.font = '30px Arial';
    ctx.textAlign = "center";
    ctx.fillText("Press 'space' to start again!", CANVAS_WIDTH / 2 , CANVAS_HEIGHT / 2 + 40 );
    ctx.textAlign = "center";
    ctx.fillText('Final score: ' + score, CANVAS_WIDTH / 2 , CANVAS_HEIGHT / 2 + 80 );
    gameOver = true;
  } else {
    ctx.fillStyle = 'white';
    ctx.font = '20px Monospace';
    ctx.fillText('SCORE:' + score, CANVAS_WIDTH - (CANVAS_WIDTH - 0.11 * CANVAS_WIDTH) , CANVAS_HEIGHT - (CANVAS_HEIGHT - 0.035 * CANVAS_HEIGHT) );
    DrawLifeShips();
  }

  if(asteroids.length !== 0) { // Check collisions
    for(asteroid in asteroids) {
      if(circleCollision(ship.x, ship.y, 11, asteroids[asteroid].x, asteroids[asteroid].y, asteroids[asteroid].collisionRadius) && !ship.invincible) {
        console.log("COLIDED");
        ship.x = CANVAS_WIDTH / 2;
        ship.y = CANVAS_HEIGHT / 2 ;
        ship.velX = 0;
        ship.velY = 0;
        lives--;
        ship.Invincibility();
        break;
      }
    }
  }

  if (asteroids.length === 0) { // Next Level
    bullets.splice(0,bullets.length);
    ship.x = CANVAS_WIDTH / 2;
    ship.y = CANVAS_HEIGHT / 2 ;
    ship.velX = 0;
    ship.velY = 0;
    spawnAsteroids(8);
    pauseRender = true;
    ctx.font = '50px Arial';
    ctx.textAlign = "center";
    ctx.fillText('Level ' + ++gameLevel, CANVAS_WIDTH / 2 , CANVAS_HEIGHT / 2 );
    setTimeout(() => {
      console.log("setTimeout");
      pauseRender = false;
      Render();
    }, 1000)
  }

  if(asteroids.length !== 0 && bullets.length !== 0) { // Check bullet collisions
    loop1:
    for(asteroid in asteroids) {
      for (bullet in bullets) {
        if (circleCollision(bullets[bullet].x, bullets[bullet].y, 3, asteroids[asteroid].x, asteroids[asteroid].y, asteroids[asteroid].collisionRadius )){
          // console.log('asteroid x ' + asteroids[asteroid].x);
          if (asteroids[asteroid].level < 3) {
            asteroids.push(new Asteroid(
              asteroids[asteroid].x + 5,
              asteroids[asteroid].y - 5, asteroids[asteroid].radius / 2, asteroids[asteroid].level + 1, asteroids[asteroid].collisionRadius / 2));
            asteroids.push(new Asteroid(
              asteroids[asteroid].x - 5,
              asteroids[asteroid].y + 5, asteroids[asteroid].radius / 2, asteroids[asteroid].level + 1 , asteroids[asteroid].collisionRadius / 2));
          }
          // console.log('asteroid x ' + asteroids[asteroids.length - 1].x);
          score += 20 + asteroids[asteroid].level * 10;
          asteroids.splice(asteroid,1);
          bullets.splice(bullet,1);
          break loop1;
        }
      }
    }
  }
  // Update logic and Draw
  ship.Update();
  if (ship.visible){
    ship.Draw();
  }
  if(bullets.length !== 0) {
    for (bullet in bullets) {
      bullets[bullet].Update();
      bullets[bullet].Draw();
    }
  }
  if (asteroids.length !== 0) {
    for (asteroid in asteroids) {
      asteroids[asteroid].Update();
      asteroids[asteroid].Draw();
    }
  }
  requestAnimationFrame(Render);
}
